package ro.ubbcluj.map.service;

import javafx.util.Pair;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Request;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.utils.events.ChangeEventType;
import ro.ubbcluj.map.utils.events.RequestEvent;
import ro.ubbcluj.map.utils.observer.Observable;
import ro.ubbcluj.map.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class RequestsService implements Observable<RequestEvent> {
    private final Repository<Long, Request> requestRepository;
    private final Repository<Long, User> userRepository;
    private final Repository<Long, Friendship> friendshipRepository;

    private final List<Observer<RequestEvent>> observers = new ArrayList<>();

    public RequestsService(Repository<Long, Request> repo,
                           Repository<Long, User> userRepository, Repository<Long, Friendship> friendshipRepository) {
        this.requestRepository = repo;
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
    }

    public void addRequest(Long id_from, Long id_to) {
        HashSet<Friendship> all_friendships = (HashSet<Friendship>) this.friendshipRepository.getAll();
        if (all_friendships.stream().noneMatch(f -> (
                Objects.equals(f.getId1(), id_from) && Objects.equals(f.getId2(), id_to)) ||
                (Objects.equals(f.getId2(), id_from) && Objects.equals(f.getId1(), id_to)))) {
            Request u = new Request(id_from, id_to, "pending", LocalDateTime.now());
            this.requestRepository.save(u);
            notifyObservers(new RequestEvent(ChangeEventType.ADD, u));
        } else {
            throw new IllegalArgumentException("This friendship already exists!");
        }
    }

    public void deleteRequest(Long id) {
        Request request = this.requestRepository.getOne(id);
        Request r = this.requestRepository.delete(id);
        if (r == null) {
            notifyObservers(new RequestEvent(ChangeEventType.DELETE, request));
        }
    }

    public void deleteARequest(Long id_from, Long id_to) {
        Request request = this.requestRepository.getOne(findID(id_from, id_to));
        Request r = this.requestRepository.delete(findID(id_from, id_to));
        if (r == null) {
            notifyObservers(new RequestEvent(ChangeEventType.DELETE, request));
        }
    }

    public void confirmRequest(Long id_from, Long id_to) {
        Request request = this.requestRepository.getOne(findID(id_from, id_to));
        request.setStatus("approved");
        request.setId(findID(id_from, id_to));

        Friendship f = new Friendship(id_from, id_to, LocalDateTime.now());
        this.friendshipRepository.save(f);

        this.deleteRequest(findID(id_from, id_to));
        notifyObservers(new RequestEvent(ChangeEventType.DELETE, request));
    }

    public void updateRequest(Long id, String status) {
        Request u = this.requestRepository.getOne(id);
        u.setStatus(status);
        u.setId(id);

        if (Objects.equals(status, "approved")) {
            Friendship f = new Friendship(u.getFrom(), u.getTo(), LocalDateTime.now());
            this.friendshipRepository.save(f);
        }

        if (Objects.equals(status, "approved") || Objects.equals(status, "rejected")) {
            this.deleteRequest(id);
            notifyObservers(new RequestEvent(ChangeEventType.DELETE, this.requestRepository.getOne(id)));
        } else if (!Objects.equals(status, "pending")) {
            throw new IllegalArgumentException("Wrong status!!!");
        }
    }

    public Iterable<Request> getAllRequests() {
        return this.requestRepository.getAll();
    }

    public Map<User, LocalDateTime> getAllRequestsForAnUser(Long id) {
        HashSet<Request> all = (HashSet<Request>) this.requestRepository.getAll();
        return all.stream().filter(r -> Objects.equals(r.getTo(), id)).
                map(x -> new Pair<>(userRepository.getOne(x.getFrom()), x.getDateTime()))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }


    public Request getOne(Long id) {
        return this.requestRepository.getOne(id);
    }

    @Override
    public void addObserver(Observer<RequestEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<RequestEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(RequestEvent t) {
        observers.forEach(x -> x.update(t));
    }

    public Long findID(Long id_from, Long id_to) {
        HashSet<Request> requests = (HashSet<Request>) this.requestRepository.getAll();
        return requests.stream().
                filter(x -> Objects.equals(x.getFrom(), id_from) && Objects.equals(x.getTo(), id_to))
                .collect(Collectors.toList()).get(0).getId();
    }

    public int existRequest(Long id1, Long id2) {//id1 a trimis cerere spre id2
        Map<User, LocalDateTime> idRequest = getAllRequestsForAnUser(id2);

        for (Map.Entry<User, LocalDateTime> request : idRequest.entrySet()) {
            if (request.getKey().getId().equals(id1))
                return 0;
        }
        return 1;
    }


}
