package ro.ubbcluj.map.service;

import org.jetbrains.annotations.NotNull;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.utils.events.ChangeEventType;
import ro.ubbcluj.map.utils.events.FriendshipEvent;
import ro.ubbcluj.map.utils.observer.Observable;
import ro.ubbcluj.map.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class FriendshipsService implements Observable<FriendshipEvent> {
    private final Repository<Long, Friendship> friendshipRepository;
    private final Repository<Long, User> userRepository;

    private final List<Observer<FriendshipEvent>> observers = new ArrayList<>();

    public FriendshipsService(Repository<Long, Friendship> repo, Repository<Long, User> repo2) {
        this.friendshipRepository = repo;
        this.userRepository = repo2;
    }

    public void addFriendship(@NotNull Long id1, @NotNull Long id2, LocalDateTime dateTime) {
        verifyFriends(id1, id2);
        Friendship friendship = new Friendship(id1, id2, dateTime);
        this.friendshipRepository.save(friendship);
        notifyObservers(new FriendshipEvent(ChangeEventType.ADD, friendship));
    }

    public void deleteFriendship(Long id) {
        Friendship f = this.friendshipRepository.delete(id);
        notifyObservers(new FriendshipEvent(ChangeEventType.DELETE, f));
    }

    public Iterable<Friendship> getAllFriendships() {
        return this.friendshipRepository.getAll();
    }

    public void deleteFriendshipsWithThisUser(Long id) {
        AtomicReference<ArrayList<Friendship>> friendships = new AtomicReference<>
                (new ArrayList<>((Collection<Friendship>) this.friendshipRepository.getAll()));
        int size = friendships.get().size();
        for (int i = 0; i < size; i++) {
            friendships.set(new ArrayList<>((Collection<Friendship>) this.friendshipRepository.getAll()));
            for (Friendship friendship : friendships.get()) {
                if (Objects.equals(friendship.getId1(), id) || Objects.equals(friendship.getId2(), id)) {
                    deleteFriendship(friendship.getId());
                    break;
                }
            }
        }
    }

    public void deleteFriendshipBetweenTwoUsers(Long id1, Long id2) {
        AtomicReference<ArrayList<Friendship>> friendships = new AtomicReference<>
                (new ArrayList<>((Collection<Friendship>) this.friendshipRepository.getAll()));
        int size = friendships.get().size();
        for (int i = 0; i < size; i++) {
            friendships.set(new ArrayList<>((Collection<Friendship>) this.friendshipRepository.getAll()));
            for (Friendship friendship : friendships.get()) {
                if ((Objects.equals(friendship.getId1(), id1) && Objects.equals(friendship.getId2(), id2))
                        || (Objects.equals(friendship.getId2(), id1) && Objects.equals(friendship.getId1(), id2))) {
                    deleteFriendship(friendship.getId());
                    break;
                }
            }
        }
    }

    public void verifyFriends(Long id1, Long id2) {
        User u1 = userRepository.getOne(id1);
        User u2 = userRepository.getOne(id2);
        if (u1 == null || u2 == null) {
            throw new IllegalArgumentException("the users should exist");
        }
    }

    @Override
    public void addObserver(Observer<FriendshipEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipEvent t) {
        observers.forEach(x -> x.update(t));
    }


}
