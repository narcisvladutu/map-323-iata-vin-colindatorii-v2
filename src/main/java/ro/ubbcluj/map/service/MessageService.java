package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.Message;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.utils.events.ChangeEventType;
import ro.ubbcluj.map.utils.events.MessageEvent;
import ro.ubbcluj.map.utils.observer.Observable;
import ro.ubbcluj.map.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MessageService implements Observable<MessageEvent> {

    private final Repository<Long, Message> messageRepository;

    private final List<Observer<MessageEvent>> observers = new ArrayList<>();

    public MessageService(Repository<Long, Message> messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void addMessage(Long from, Long to, String messageText, LocalDateTime date, Long idReplay) {
        Message message = new Message(from, to, messageText, date, idReplay);
        this.messageRepository.save(message);
        notifyObservers(new MessageEvent(ChangeEventType.ADD, message));
    }

    public void replyAll(Long from, List<Long> ids_to, String messageText, LocalDateTime dateTime, List<Long> idsReply) {
        int i = 0;
        for (Long to : ids_to) {
            Message message = new Message(from, to, messageText, dateTime, idsReply.get(i));
            this.messageRepository.save(message);
            notifyObservers(new MessageEvent(ChangeEventType.ADD, message));
            i++;
        }
    }

    public void deleteMessage(Long id) {
        Message message = this.messageRepository.delete(id);
        notifyObservers(new MessageEvent(ChangeEventType.ADD, message));
    }

    public Iterable<Message> getAllMessages() {
        return this.messageRepository.getAll();
    }

    public Message getOne(Long id) {
        return this.messageRepository.getOne(id);
    }

    public static Stream<Message> getStreamFromIterable(Iterable<Message> iterable) {
        Spliterator<Message> spliterator = iterable.spliterator();
        return StreamSupport.stream(spliterator, false);
    }

    public void showConversation(Long first_id, Long second_id) {
        Iterable<Message> messagesSet = this.messageRepository.getAll();
        Stream<Message> streamMessageSet = getStreamFromIterable(messagesSet);
        Predicate<Message> p1 = message -> {
            try {
                return (Objects.equals(message.getTo(), first_id) && Objects.equals(message.getFrom(), second_id)) || (Objects.equals(message.getTo(), second_id) && Objects.equals(message.getFrom(), first_id));
            } catch (NumberFormatException ex) {
                return true;
            }
        };
        streamMessageSet.filter(p1).sorted(Comparator.comparing(Message::getData)).forEach(System.out::println);
    }

    public Stream<Message> getConversation(Long first_id, Long second_id) {
        Iterable<Message> messagesSet = this.messageRepository.getAll();
        Stream<Message> streamMessageSet = getStreamFromIterable(messagesSet);
        Predicate<Message> p1 = message -> {
            try {
                return (Objects.equals(message.getTo(), first_id) && Objects.equals(message.getFrom(), second_id)) || (Objects.equals(message.getTo(), second_id) && Objects.equals(message.getFrom(), first_id));
            } catch (NumberFormatException ex) {
                return true;
            }
        };
        return streamMessageSet.filter(p1).sorted(Comparator.comparing(Message::getData));
    }

    @Override
    public void addObserver(Observer<MessageEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<MessageEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(MessageEvent t) {
        observers.forEach(x -> x.update(t));
    }
}
