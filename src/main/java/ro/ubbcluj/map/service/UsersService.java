package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.utils.events.UserEvent;
import ro.ubbcluj.map.utils.observer.Observable;
import ro.ubbcluj.map.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;


public class UsersService implements Observable<UserEvent> {
    private final Repository<Long, User> userRepository;
    private final List<Observer<UserEvent>> observers = new ArrayList<>();


    public UsersService(Repository<Long, User> repo) {
        this.userRepository = repo;
    }

    public void addUser(String first_name, String last_name, String email, String password, String gender) {
        User u = new User(first_name, last_name, email, password, gender);
        this.userRepository.save(u);
    }

    public void addUser(User u) {
        this.userRepository.save(u);
    }

    public void deleteUser(Long id) {
        this.userRepository.delete(id);
    }

    public void updateUser(Long id, String first_name, String last_name, String email, String password, String gender) {
        User u = new User(first_name, last_name, email, password, gender);
        u.setId(id);
        this.userRepository.update(u);
    }

    public Iterable<User> getAllUsers() {
        return this.userRepository.getAll();
    }

    public User getOne(Long id) {
        return this.userRepository.getOne(id);
    }

    @Override
    public void addObserver(Observer<UserEvent> e) {
        observers.add(e);
    }


    @Override
    public void removeObserver(Observer<UserEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(UserEvent t) {
        observers.forEach(x -> x.update(t));
    }
}