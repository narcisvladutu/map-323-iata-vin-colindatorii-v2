package ro.ubbcluj.map.utils;

import javafx.scene.paint.Color;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constants {
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm");
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final LocalDateTime CURRENT_TIME = LocalDateTime.now();

    public static InputStream input = null;

    public static String url = null;
    public static String username = null;
    public static String password = null;

    static {
        try {
            input = new FileInputStream(
                    "src\\main\\resources\\ro\\ubbcluj\\map\\config.properties");
            Properties  properties;
            properties = new Properties();

            properties.load(input);

            url = properties.getProperty("url");
            username = properties.getProperty("username");
            password = properties.getProperty("password");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
/*
    CULORI

    verde_deschis = #99c7a8
    verde_inchis = #43787d
    roz_deschis = #d44f59
    roz_inchis = #bc4f56
    maro = #49232d
    maro inchis = #422c36
    crem = #f5dab1
    crem inchis = #ebcdad
    negru = #2f2629
    verdele_care_imi_place_mie_mult = #182D2C
    
 */
}