package ro.ubbcluj.map.ui;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.service.*;
import ro.ubbcluj.map.domain.validators.ValidationException;
import ro.ubbcluj.map.utils.Constants;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

public class UI {
    Scanner scanner = new Scanner(System.in);
    UsersService usersService;
    FriendshipsService friendshipsService;
    MessageService messageService;
    WorldService worldService;
    RequestsService requestsService;

    public UI(UsersService usersService, FriendshipsService friendshipsService, WorldService worldService,
              MessageService messageService, RequestsService requestsService) {
        this.usersService = usersService;
        this.friendshipsService = friendshipsService;
        this.worldService = worldService;
        this.messageService = messageService;
        this.requestsService = requestsService;
    }

    public void menu() {
        AtomicReference<String> choice = new AtomicReference<>("0");
        while (!Objects.equals(choice.get(), "x")) {
            printMenu();
            choice.set(scanner.nextLine());
            System.out.println("\n");
            switch (choice.get()) {
                case "1" -> addUser();
                case "2" -> deleteUser();
                case "3" -> updateUser();
                case "4" -> showUsers();
                case "5" -> addFriendship();
                case "6" -> deleteFriendship();
                case "7" -> showFriendship();
                case "8" -> showNumberOfCommunities();
                case "9" -> showBiggestCommunity();
                case "10" -> addMessage();
                case "11" -> deleteMessage();
                case "12" -> showMessage();
                case "13" -> showConversation();
                case "14" -> showFriendshipsForAnUser();
                case "15" -> showFriendshipsForAnUserFromAMonth();
                case "16" -> addFriendRequest();
                case "17" -> showAllFriendRequests();
                case "18" -> showAllFriendRequestsForAnUser();
                case "19" -> acceptOrRejectRequest();
                case "x" -> System.out.println("GOODBYE");
                default -> System.out.println(Constants.ANSI_RED + "INCORRECT" + Constants.ANSI_RESET);
            }
        }
    }


    private void printMenu() {
        System.out.println("\n");
        System.out.println("1. Add an user");
        System.out.println("2. Delete an user");
        System.out.println("3. Update an user");
        System.out.println("4. Show all users");
        System.out.println("5. Add a friendship");
        System.out.println("6. Delete a friendship");
        System.out.println("7. Show all friendships");
        System.out.println("8. Show the number of communities");
        System.out.println("9. Show the biggest community");
        System.out.println("10.Add a message");
        System.out.println("11.Delete a message");
        System.out.println("12.Show all messages");
        System.out.println("13.Show the conversation of two users");
        System.out.println("14.Show the friends of an user");
        System.out.println("15.Show the friends of an user from a month");
        System.out.println("16.Add a friend request");
        System.out.println("17.Show all friend requests");
        System.out.println("18.Show all friend requests for a user");
        System.out.println("19.Accept or reject a friend request");
        System.out.println("x. EXIT");
        System.out.println("Select your choice: ");
    }

    private void addUser() {
        System.out.println("Give the first name");
        String first_name = scanner.nextLine();
        System.out.println("Give the last name");
        String last_name = scanner.nextLine();
        System.out.println("Give the email");
        String email = scanner.nextLine();
        System.out.println("Give the password");
        String password = scanner.nextLine();
        System.out.println("Give the gender");
        String gender = scanner.nextLine();
        try {
            this.usersService.addUser(first_name, last_name, email, password, gender);
            System.out.println("USER ADDED SUCCESSFULLY");
        } catch (ValidationException | IllegalArgumentException validationException) {
            System.out.println(Constants.ANSI_RED + "ERROR: " + Constants.ANSI_RESET);
            System.out.println(validationException.getMessage());
        }
        scanner.nextLine();
    }

    private void updateUser() {
        System.out.println("Give the id");
        Long id = Long.valueOf(scanner.nextLine());
        System.out.println("Give the new first name");
        String first_name = scanner.nextLine();
        System.out.println("Give the new last name");
        String last_name = scanner.nextLine();
        System.out.println("Give the new email");
        String email = scanner.nextLine();
        System.out.println("Give the new password");
        String password = scanner.nextLine();
        System.out.println("Give the gender");
        String gender = scanner.nextLine();
        try {
            this.usersService.updateUser(id, first_name, last_name, email, password, gender);
            System.out.println("USER UPDATED SUCCESSFULLY");
        } catch (ValidationException | IllegalArgumentException validationException) {
            System.out.println(Constants.ANSI_RED + "ERROR: " + Constants.ANSI_RESET);
            System.out.println(validationException.getMessage());
        }
        scanner.nextLine();
    }

    private void deleteUser() {
        System.out.println("Give the id");
        Long id = Long.parseLong(scanner.nextLine());
        this.friendshipsService.deleteFriendshipsWithThisUser(id);
        this.usersService.deleteUser(id);
        System.out.println("USER DELETED SUCCESSFULLY");
        scanner.nextLine();
    }

    private void addFriendship() {
        System.out.println("Give the first id");
        Long id1 = Long.parseLong(scanner.next());
        System.out.println("Give the second id");
        Long id2 = Long.parseLong(scanner.next());
        try {
            this.friendshipsService.addFriendship(id1, id2, Constants.CURRENT_TIME);
            System.out.println("FRIENDSHIP ADDED SUCCESSFULLY");
        } catch (ValidationException | IllegalArgumentException validationException) {
            System.out.println(Constants.ANSI_RED + "ERROR: " + Constants.ANSI_RESET);
            System.out.println(validationException.getMessage());
        }
        scanner.nextLine();
    }

    private void deleteFriendship() {
        System.out.println("Give the id");
        Long id = Long.parseLong(scanner.nextLine());
        this.friendshipsService.deleteFriendship(id);
        System.out.println("FRIENDSHIP DELETED SUCCESSFULLY");
        scanner.nextLine();
    }

    private void showUsers() {
        this.usersService.getAllUsers().forEach(System.out::println);
    }

    private void showFriendship() {
        this.friendshipsService.getAllFriendships().forEach(System.out::println);
    }

    private void showNumberOfCommunities() {
        System.out.println(worldService.numberOfCommunities());
    }

    private void showBiggestCommunity() {
        this.worldService.getBiggestCommunity().forEach(System.out::println);
    }

    private void addMessage() {
        System.out.println("from: ");
        Long from = Long.parseLong(scanner.nextLine());

        System.out.println("to: ");
        Long to = Long.parseLong(scanner.nextLine());

        System.out.println("Message");
        String messageText = scanner.nextLine();

        System.out.println("id replay: ");
        Long idReplay = Long.parseLong(scanner.nextLine());

        try {
            this.messageService.addMessage(
                    from, to, messageText, Constants.CURRENT_TIME, idReplay);
            System.out.println("MESSAGE ADDED SUCCESSFULLY");
        } catch (ValidationException | IllegalArgumentException validationException) {
            System.out.println(Constants.ANSI_RED + "ERROR: " + Constants.ANSI_RESET);
            System.out.println(validationException.getMessage());
        }
        scanner.nextLine();
    }

    private void deleteMessage() {
        System.out.println("Give the id");
        Long id = Long.parseLong(scanner.nextLine());
        this.messageService.deleteMessage(id);
        System.out.println("MESSAGE DELETED SUCCESSFULLY");
        scanner.nextLine();
    }

    private void showMessage() {
        this.messageService.getAllMessages().forEach(System.out::println);
    }

    private void showConversation() {
        System.out.println("Give the first id");
        Long first_id = Long.parseLong(scanner.nextLine());
        System.out.println("Give the second id");
        Long second_id = Long.parseLong(scanner.nextLine());

        this.messageService.showConversation(first_id, second_id);
    }

    private void showFriendshipsForAnUser() {
        System.out.println("Give the id of the user");
        Long id = Long.parseLong(scanner.nextLine());
        Map<User, LocalDateTime> friends = this.worldService.friendshipsOfAnUser(id);
        for (Map.Entry<User, LocalDateTime> friend : friends.entrySet()) {
            System.out.println(friend.getKey().getFirstName() + "|" + friend.getKey().getLastName() + "|" +
                    friend.getValue().format(Constants.DATE_TIME_FORMATTER));
        }
    }

    private void showFriendshipsForAnUserFromAMonth() {
        System.out.println("Give the id of the user: ");
        Long id = Long.parseLong(scanner.nextLine());
        System.out.println("Give the month: ");
        int month = Integer.parseInt(scanner.nextLine());
        Map<User, LocalDateTime> friends = this.worldService.friendshipsOfAnUser(id, month);
        for (Map.Entry<User, LocalDateTime> friend : friends.entrySet()) {
            System.out.println(friend.getKey().getFirstName() + "|" + friend.getKey().getLastName() + "|" +
                    friend.getValue().format(Constants.DATE_TIME_FORMATTER));
        }
    }

    private void addFriendRequest() {
        System.out.println("from: ");
        Long from = Long.parseLong(scanner.nextLine());

        System.out.println("to: ");
        Long to = Long.parseLong(scanner.nextLine());

        try {
            this.requestsService.addRequest(from, to);
            System.out.println("REQUEST SENT SUCCESSFULLY");
        } catch (ValidationException | IllegalArgumentException validationException) {
            System.out.println(Constants.ANSI_RED + "ERROR: " + Constants.ANSI_RESET);
            System.out.println(validationException.getMessage());
        }
        scanner.nextLine();
    }

    private void showAllFriendRequests() {
        this.requestsService.getAllRequests().forEach(System.out::println);
    }

    private void showAllFriendRequestsForAnUser() {
        System.out.println("Give the id of the user");
        Long id = Long.parseLong(scanner.nextLine());
        System.out.println(this.requestsService.getAllRequestsForAnUser(id));
    }

    private void acceptOrRejectRequest() {
        System.out.println("Give the id of the friend request:");
        Long id = Long.parseLong(scanner.nextLine());

        System.out.println("Do you want to accept it or to reject it?");
        String status = scanner.nextLine();

        try {
            this.requestsService.updateRequest(id, status);
        } catch (ValidationException | IllegalArgumentException validationException) {
            System.out.println(Constants.ANSI_RED + "ERROR: " + Constants.ANSI_RESET);
            System.out.println(validationException.getMessage());
        }
    }
}