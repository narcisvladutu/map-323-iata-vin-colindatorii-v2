package ro.ubbcluj.map.repo.memory;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.validators.Validator;
import ro.ubbcluj.map.repo.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {

    private final Validator<E> validator;
    protected Map<ID, E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    @Override
    public E getOne(ID id) {
        if (id == null)
            throw new IllegalArgumentException("id must be not null");
        return entities.get(id);
    }

    @Override
    public Iterable<E> getAll() {
        return entities.values();
    }

    public int size() {
        return entities.size();
    }

    @Override
    public void verifyEntity(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        if (entities.get(entity.getId()) != null) {
            throw new IllegalArgumentException("the id must be unique");
        }
    }

    @Override
    public E save(E entity) {
        verifyEntity(entity);
        entities.put(entity.getId(), entity);
        return null;
    }

    @Override
    public E delete(ID id) {
        E e = this.entities.get(id);
        if (e == null) {
            throw new IllegalArgumentException("the entity should exist!");
        } else {
            this.entities.remove(id);
            return e;
        }
    }

    @Override
    public E update(E entity) {

        if (entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);

        if (entities.get(entity.getId()) != null) {
            entities.put(entity.getId(), entity);
            return entity;
        }
        return null;
    }
}
