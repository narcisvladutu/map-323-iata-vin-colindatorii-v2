package ro.ubbcluj.map.repo.file;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.Validator;

import java.util.List;

public class UserRepository extends AbstractFileRepository<Long, User> {

    public UserRepository(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    @Override
    public User extractEntity(List<String> attributes) {
        User u = new User(attributes.get(1), attributes.get(2),attributes.get(3),attributes.get(4), attributes.get(3));
        u.setId(Long.parseLong(attributes.get(0)));
        return u;
    }

    @Override
    public String createEntityAsString(User entity) {
        return entity.getId().toString() + ";" + entity.getFirstName() + ";" + entity.getLastName()+";"+entity.getEmail()+";"+entity.getPassword();
    }
}

