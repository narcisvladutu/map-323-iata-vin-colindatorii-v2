package ro.ubbcluj.map.repo.file;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class FriendshipsRepository extends AbstractFileRepository<Long, Friendship> {
    public FriendshipsRepository(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    @Override
    public Friendship extractEntity(List<String> attributes) {
        Friendship friendship = new Friendship(Long.parseLong(attributes.get(1)), Long.parseLong(attributes.get(2)),
                LocalDateTime.parse(attributes.get(3)));
        friendship.setId(Long.parseLong(attributes.get(0)));
        return friendship;
    }

    @Override
    public String createEntityAsString(Friendship entity) {
        return entity.getId().toString() + ";" + entity.getId1().toString() + ";" + entity.getId2().toString() + ";"
                + entity.getDate();
    }

    @Override
    public void verifyEntity(Friendship entity) {
        super.verifyEntity(entity);
        for (Friendship friendship : getAll()) {
            if ((Objects.equals(friendship.getId1(), entity.getId1()) &&
                    Objects.equals(entity.getId2(), friendship.getId2())) ||
                    (Objects.equals(friendship.getId2(), entity.getId1()) &&
                            Objects.equals(entity.getId2(), friendship.getId1()))) {
                throw new IllegalArgumentException("this friendship already exists");
            }
        }
    }
}