package ro.ubbcluj.map.domain;

import ro.ubbcluj.map.utils.Constants;

import java.time.LocalDateTime;
import java.util.Objects;

public class Friendship extends Entity<Long> {
    private final Long id1;
    private final Long id2;
    private final LocalDateTime date;

    public Friendship(Long id1, Long id2, LocalDateTime date) {
        this.id1 = Objects.requireNonNull(id1);
        this.id2 = Objects.requireNonNull(id2);
        this.date = date;
    }

    public Long getId1() {
        return id1;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id1=" + id1 +
                ", id2=" + id2 +
                ", date=" + date.format(Constants.DATE_TIME_FORMATTER) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return Objects.equals(id1, that.id1) && Objects.equals(id2, that.id2) && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id1, id2, date);
    }

    public Long getId2() {
        return id2;
    }
}
