package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.Friendship;

public class FriendshipValidator implements Validator<Friendship> {
    @Override
    public void validate(Friendship entity) throws ValidationException {
        if (entity.getId() == null) throw new ValidationException("The ID shouldn't be null");

        if (entity.getId1() == null) throw new ValidationException("The ID shouldn't be null");

        if (entity.getId2() == null) throw new ValidationException("The ID shouldn't be null");
    }
}
