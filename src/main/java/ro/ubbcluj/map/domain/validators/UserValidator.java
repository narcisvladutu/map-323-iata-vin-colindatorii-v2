package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(entity.getEmail());
        if (!matcher.matches())
            throw new ValidationException("Email format is not valid");

        char[] array = new char[entity.getFirstName().length()];
        entity.getFirstName().getChars(0, entity.getFirstName().length(),
                array, 0);
        for (char c : array) {
            if (!Character.isLetter(c) && c != ' ') {
                throw new ValidationException("The first name can contain only letters");
            }
        }

        char[] array2 = new char[entity.getLastName().length()];
        entity.getLastName().getChars(0, entity.getLastName().length(),
                array2, 0);
        for (char c : array2) {
            if (!Character.isLetter(c) && c != ' ') {
                throw new ValidationException("The last name can contain only letters");
            }
        }
    }
}