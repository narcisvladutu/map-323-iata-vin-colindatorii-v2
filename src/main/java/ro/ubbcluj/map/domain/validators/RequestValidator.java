package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.Request;

import java.util.Objects;

public class RequestValidator implements Validator<Request> {


    @Override
    public void validate(Request entity) throws ValidationException {
        if (entity.getId() == null) throw new ValidationException("The ID shouldn't be null");

        if (entity.getTo() == null) throw new ValidationException("To: shouldn't be null");

        if (entity.getFrom() == null) throw new ValidationException("From: shouldn't be null");

        if (!Objects.equals(entity.getStatus(), "approved")
                && !Objects.equals(entity.getStatus(), "rejected") &&
                !Objects.equals(entity.getStatus(), "pending"))
            throw new ValidationException("Status should be only approved, rejected or pending");
    }
}
