package ro.ubbcluj.map;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.repo.database.UserDatabaseRepository;
import ro.ubbcluj.map.service.UsersService;
import ro.ubbcluj.map.utils.Constants;

import java.io.IOException;

public class LogInApplication extends Application {
       Repository<Long, User> userRepository = new UserDatabaseRepository(Constants.url,Constants.username, Constants.password, new UserValidator());


    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/logIn-view.fxml"));
        Parent root = fxmlLoader.load();
        LogInController ctrl=fxmlLoader.getController();
        ctrl.setService(new UsersService(userRepository));

        Scene scene = new Scene(root, 550, 600);
        stage.setTitle("Aho, aho!");
        stage.setScene(scene);
        stage.show();
        stage.getIcons().add(new Image("ro/ubbcluj/map/Images/logo.png"));
    }

    public static void main(String[] args) {
        launch();
    }
}