package ro.ubbcluj.map;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Request;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipValidator;
import ro.ubbcluj.map.domain.validators.RequestValidator;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.repo.database.FriendshipDatabaseRepository;
import ro.ubbcluj.map.repo.database.RequestDatabaseRepository;
import ro.ubbcluj.map.repo.database.UserDatabaseRepository;
import ro.ubbcluj.map.service.FriendshipsService;
import ro.ubbcluj.map.service.RequestsService;
import ro.ubbcluj.map.service.UsersService;
import ro.ubbcluj.map.service.WorldService;
import ro.ubbcluj.map.utils.Constants;

import java.io.IOException;
import java.util.Objects;

public class ProfileController {

    @FXML
    Button signOut;
    @FXML
    Button profileMyFriends;
    @FXML
    Button profileAllUsers;
    @FXML
    Button profileNews;
    @FXML
    ImageView profileImage;
    @FXML
    Label profileFirstName;
    @FXML
    Label profileSecondName;
    @FXML
    Label profileMail;

    Long userID;
    UsersService service;
    private Stage stage;
    private Scene scene;

    Repository<Long, User> userRepository = new UserDatabaseRepository(Constants.url, Constants.username,
            Constants.password, new UserValidator());
    Repository<Long, Friendship> friendshipRepository = new FriendshipDatabaseRepository(Constants.url, Constants.username,
            Constants.password, new FriendshipValidator());
    Repository<Long, Request> requestRepository = new RequestDatabaseRepository(Constants.url, Constants.username,
            Constants.password, new RequestValidator());

    public void setService(UsersService service, Long user) {
        this.service = service;
        this.userID = user;
        setData();
    }

    @FXML
    private void initialize() {
    }

    @FXML
    private void setData() {
        profileFirstName.setText(service.getOne(userID).getFirstName());
        profileSecondName.setText(service.getOne(userID).getLastName());
        profileMail.setText(service.getOne(userID).getEmail());
        if (Objects.equals(service.getOne(userID).getGender(), "female")) {
            profileImage.setImage(new Image("ro/ubbcluj/map/Images/f.jpg"));
        }
    }

    public void onMyFriendsButton(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/my_friends-view.fxml"));
        Parent root = fxmlLoader.load();
        MyFriendsController ctrl = fxmlLoader.getController();
        ctrl.setService(new UsersService(userRepository), new
                        WorldService(friendshipRepository, userRepository),
                new FriendshipsService(friendshipRepository, userRepository), userID);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void onAllUsersButton(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/allUsers-view.fxml"));
        Parent root = fxmlLoader.load();
        AllUsersController ctrl = fxmlLoader.getController();
        ctrl.setService(new
                        WorldService(friendshipRepository, userRepository),
                new UsersService(userRepository), userID);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void onProfileNews(ActionEvent event) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/friend_requests.fxml"));
        Parent root = fxmlLoader.load();
        FriendsRequestsController ctrl = fxmlLoader.getController();
        ctrl.setService(new
                RequestsService(requestRepository, userRepository, friendshipRepository), new UsersService(userRepository), userID);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void onSignOut(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/logIn-view.fxml"));
        Parent root = fxmlLoader.load();
        LogInController ctrl = fxmlLoader.getController();
        ctrl.setService(service);
        stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 550, 600);
        stage.setScene(scene);
        stage.show();
    }
}