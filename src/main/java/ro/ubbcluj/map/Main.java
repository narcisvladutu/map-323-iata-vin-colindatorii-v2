package ro.ubbcluj.map;

import ro.ubbcluj.map.domain.Request;
import ro.ubbcluj.map.domain.validators.MessageValidator;
import ro.ubbcluj.map.domain.validators.RequestValidator;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repo.database.RequestDatabaseRepository;
import ro.ubbcluj.map.service.*;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Message;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipValidator;
import ro.ubbcluj.map.repo.Repository;
import ro.ubbcluj.map.repo.database.FriendshipDatabaseRepository;
import ro.ubbcluj.map.repo.database.MessageDatabaseRepository;
import ro.ubbcluj.map.repo.database.UserDatabaseRepository;
import ro.ubbcluj.map.ui.UI;
import ro.ubbcluj.map.utils.Constants;

public class Main {

    public static void main(String[] args) {
        Repository<Long, User> userRepository = new UserDatabaseRepository(Constants.url, Constants.username, Constants.password, new UserValidator());
        UsersService usersService = new UsersService(userRepository);
        Repository<Long, Friendship> friendshipRepository = new FriendshipDatabaseRepository(Constants.url, Constants.username, Constants.password, new FriendshipValidator());
        FriendshipsService friendshipsService = new FriendshipsService(friendshipRepository, userRepository);
        WorldService worldService = new WorldService(friendshipRepository, userRepository);

        Repository<Long, Message> messageRepository = new MessageDatabaseRepository(Constants.url, Constants.username, Constants.password, new MessageValidator());
        MessageService messageService = new MessageService(messageRepository);

        Repository<Long, Request> requestRepository = new RequestDatabaseRepository(Constants.url, Constants.username, Constants.password, new RequestValidator());
        RequestsService requestsService = new RequestsService(requestRepository, userRepository, friendshipRepository);

        UI ui = new UI(usersService, friendshipsService, worldService, messageService, requestsService);
        //ui.menu();
        LogInApplication.main(args);
    }
}
