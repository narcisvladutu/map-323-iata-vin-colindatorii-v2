module ro.ubbcluj.map {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires org.jetbrains.annotations;
    requires java.sql;

    opens ro.ubbcluj.map to javafx.fxml;
    exports ro.ubbcluj.map;

    opens ro.ubbcluj.map.domain to java.base;
    exports ro.ubbcluj.map.domain;
    exports ro.ubbcluj.map.repo;
    exports ro.ubbcluj.map.service;
    exports ro.ubbcluj.map.utils.events;
    exports ro.ubbcluj.map.utils.observer;
    exports ro.ubbcluj.map.utils;
    opens ro.ubbcluj.map.utils to javafx.fxml;
}